# Strapi Postgres админ-панель

## Как развернуть проект

1. Создать файл .env по примеру из файла .env.example
2. Задать уникальные постфиксы (имена) в файле `docker-compose.yaml` для
    - сервиса `strapi-app`
    - образа `strapi-app` (совпадает с названием сервиса из пункта выше)
    - cервиса `pg`
    - поменять хост базы данных в `.env` DATABASE_HOST (совпадает с названием сервиса БД из пункта выше
    )
    - volume `strapi-data` для БД
    - подсети `strapi_app_net`
3. Указать свободные внешние порты для сервисов `strapi-app` (default `9060`) и `pg` (default `5432`)
4. Запустить команду `npm run docker-run`

### Пример переименованного docker-compose

```
version: "3"
services:
  strapi-new-app:
    build:
      context: .
      dockerfile: Dockerfile
    image: strapi-new-app
    restart: unless-stopped
    env_file: .env
    volumes:
      - ./config:/projects/app/config
      - ./src:/projects/app/src
      - ./package.json:/projects/package.json
      - ./.env:/projects/app/.env
      - ./public/uploads:/projects/app/public/uploads
    ports:
      - "9061:1337"
    networks:
      - strapi_new_app_net
    depends_on:
      - pg_new

  pg_new:
    platform: linux/amd64
    restart: unless-stopped
    env_file: .env
    image: postgres:12.0-alpine
    environment:
       POSTGRES_USER: ${DATABASE_USERNAME}
       POSTGRES_PASSWORD: ${DATABASE_PASSWORD}
       POSTGRES_DB: ${DATABASE_NAME}
    volumes:
      - strapi-new-data:/var/lib/postgresql/data/

    ports:
      - "5433:5432"
    networks:
      - strapi_new_app_net

volumes:
  strapi-new-data:

networks:
  strapi_new_app_net:
    driver: bridge