module.exports = {
  'schemas-to-ts': {
    enabled: true,
  },
  'import-export-entries': {
    enabled: true,
    config: {
      // See `Config` section.
    },
  },
};
